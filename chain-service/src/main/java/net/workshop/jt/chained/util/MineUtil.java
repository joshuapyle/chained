package net.workshop.jt.chained.util;

import net.workshop.jt.chained.model.Block;

import java.util.function.Function;

public class MineUtil {

    public static final Function<Block, String> mineFunction = block -> {
        block.setMerkleRoot(HashUtil.getMerkleRoot(block.getTransactions()));
        String hash = block.getHash();
        //Create a string with difficulty * "0"
        String target = HashUtil.getDificultyString(5);
        int nonce = 0;
        while (!hash.substring(0, 5).equals(target)) {
            block.setNonce(nonce++);
            hash = block.calculateHash();
        }
        return hash;
    };

}
