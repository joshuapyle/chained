package net.workshop.jt.chained.model;

import net.workshop.jt.chained.util.HashUtil;
import net.workshop.jt.chained.util.MineUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Chain {

    public static final float MINIMUM_TRANSACTION = 0.01f;
    private ArrayList<Block> blocks = new ArrayList<>();

    // list of all unspent transactions.
    private static HashMap<String,TransactionOutput> UTXOs = new HashMap<>();

    public static final int DIFFICULTY = 5;

    public static Boolean isChainValid(List<Block> blocks) {
        Block currentBlock;
        Block previousBlock;

        //loop through blockchain to check hashes:
        for (int i = 1; i < blocks.size(); i++) {
            currentBlock = blocks.get(i);
            previousBlock = blocks.get(i - 1);
            //compare registered hash and calculated hash:
            if (!currentBlock.getHash().equals(currentBlock.calculateHash())) {
                System.out.println("Current Hashes not equal");
                return false;
            }
            //compare previous hash and registered previous hash
            if (!previousBlock.getHash().equals(currentBlock.getPreviousHash())) {
                System.out.println("Previous Hashes not equal");
                return false;
            }
        }
        return true;
    }

    public ArrayList<Block> getBlocks() {
        return blocks;
    }

    public void setBlocks(ArrayList<Block> blocks) {
        this.blocks = blocks;
    }

    public static HashMap<String, TransactionOutput> getUTXOs() {
        return UTXOs;
    }

    public static void setUTXOs(HashMap<String, TransactionOutput> UTXOs) {
        Chain.UTXOs = UTXOs;
    }

    public Boolean isChainValid(Transaction genesisTransaction) {
        Block currentBlock;
        Block previousBlock;
        String hashTarget = HashUtil.getDificultyString(DIFFICULTY);
        //a temporary working list of unspent transactions at a given block state.
        HashMap<String,TransactionOutput> tempUTXOs = new HashMap<>();
        tempUTXOs.put(genesisTransaction.getOutputs().get(0).getId(), genesisTransaction.getOutputs().get(0));

        //loop through blockchain to check hashes:
        for(int i=1; i < blocks.size(); i++) {

            currentBlock = blocks.get(i);
            previousBlock = blocks.get(i-1);
            //compare registered hash and calculated hash:
            if(!currentBlock.getHash().equals(currentBlock.calculateHash()) ){
                System.out.println("#Current Hashes not equal");
                return false;
            }
            //compare previous hash and registered previous hash
            if(!previousBlock.getHash().equals(currentBlock.getPreviousHash()) ) {
                System.out.println("#Previous Hashes not equal");
                return false;
            }
            //check if hash is solved
            if(!currentBlock.getHash().substring( 0, DIFFICULTY).equals(hashTarget)) {
                System.out.println("#This block hasn't been mined");
                return false;
            }

            //loop thru blockchains transactions:
            TransactionOutput tempOutput;
            for(int t=0; t < currentBlock.getTransactions().size(); t++) {
                Transaction currentTransaction = currentBlock.getTransactions().get(t);

                if(!currentTransaction.verifiySignature()) {
                    System.out.println("#Signature on Transaction(" + t + ") is Invalid");
                    return false;
                }
                if(currentTransaction.getInputsValue() != currentTransaction.getOutputsValue()) {
                    System.out.println("#Inputs are note equal to outputs on Transaction(" + t + ")");
                    return false;
                }

                for(TransactionInput input: currentTransaction.getInputs()) {
                    tempOutput = tempUTXOs.get(input.getTransactionOutputId());

                    if(tempOutput == null) {
                        System.out.println("#Referenced input on Transaction(" + t + ") is Missing");
                        return false;
                    }

                    if(input.getUTXO() != null) {
                        if (input.getUTXO().getValue() != tempOutput.getValue()) {
                            System.out.println("#Referenced input Transaction(" + t + ") value is Invalid");
                            return false;
                        }
                    }

                    tempUTXOs.remove(input.getTransactionOutputId());
                }

                for(TransactionOutput output: currentTransaction.getOutputs()) {
                    tempUTXOs.put(output.getId(), output);
                }

                if(currentTransaction.getOutputs().size() > 0) {
                    if (currentTransaction.getOutputs().get(0).getRecipient() != currentTransaction.getRecipient()) {
                        System.out.println("#Transaction(" + t + ") output recipient is not who it should be");
                        return false;
                    }
                    if (currentTransaction.getOutputs().get(1).getRecipient() != currentTransaction.getSender()) {
                        System.out.println("#Transaction(" + t + ") output 'change' is not sender.");
                        return false;
                    }
                }
            }

        }
        System.out.println("Blockchain is valid");
        return true;
    }

    public void addBlock(Block newBlock) {
        newBlock.mineBlock(MineUtil.mineFunction);
        blocks.add(newBlock);
    }
}
