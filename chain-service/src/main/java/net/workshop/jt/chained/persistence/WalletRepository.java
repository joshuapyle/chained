package net.workshop.jt.chained.persistence;

import net.workshop.jt.chained.entity.WalletEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WalletRepository extends PagingAndSortingRepository<WalletEntity, String> {
}
