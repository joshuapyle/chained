package net.workshop.jt.chained.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "WALLET")
public class WalletEntity {

    @Id
    private String uuid;

    @Lob
    private String privateKey;

    @Lob
    private String publicKey;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WalletEntity that = (WalletEntity) o;
        return Objects.equals(uuid, that.uuid) &&
                Objects.equals(privateKey, that.privateKey) &&
                Objects.equals(publicKey, that.publicKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, privateKey, publicKey);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", WalletEntity.class.getSimpleName() + "[", "]")
                .add("uuid='" + uuid + "'")
                .add("privateKey='" + privateKey + "'")
                .add("publicKey='" + publicKey + "'")
                .toString();
    }
}
