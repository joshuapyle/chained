package net.workshop.jt.chained.config.orika;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

public class PrivateKeyConverter  extends BidirectionalConverter<PrivateKey, String> {


    @Override
    public String convertTo(PrivateKey source, Type<String> destinationType, MappingContext mappingContext) {
        if(source == null){
            return null;
        }
        return Base64.getEncoder().encodeToString(source.getEncoded());
    }

    @Override
    public PrivateKey convertFrom(String source, Type<PrivateKey> destinationType, MappingContext mappingContext) {
        try {
            KeyFactory ecKeyFac = KeyFactory.getInstance("ECDSA", "BC");
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(source));
            return ecKeyFac.generatePrivate(pkcs8EncodedKeySpec);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("Invalid Key Spec", e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("No Such Algorithm", e);
        } catch (NoSuchProviderException e) {
            throw new RuntimeException("No ECDSA BC Provider", e);
        }
    }
}