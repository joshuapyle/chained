package net.workshop.jt.chained.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import net.workshop.jt.chained.config.jackson.PrivateKeyDeserializer;
import net.workshop.jt.chained.config.jackson.PrivateKeySerializer;
import net.workshop.jt.chained.config.jackson.PublicKeyDeserializer;
import net.workshop.jt.chained.config.jackson.PublicKeySerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.security.PrivateKey;
import java.security.PublicKey;

@Configuration
public class JacksonConfig {

    @Bean
    public Jackson2ObjectMapperBuilder objectMapperBuilder() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.deserializerByType(PublicKey.class, new PublicKeyDeserializer());
        builder.serializerByType(PublicKey.class, new PublicKeySerializer());
        builder.deserializerByType(PrivateKey.class, new PrivateKeyDeserializer());
        builder.serializerByType(PrivateKey.class, new PrivateKeySerializer());
        builder.serializationInclusion(JsonInclude.Include.NON_NULL);
        return builder;
    }

}
