package net.workshop.jt.chained.service;

import net.workshop.jt.chained.model.Wallet;

public interface WalletService {


    Wallet createWallet();

    Wallet getWallet(String uuid);

    void deleteWallet(String uuid);

    Wallet update(String uuid, Wallet wallet);

    Wallet create(Wallet wallet);

    void delete(String uuid);
}
