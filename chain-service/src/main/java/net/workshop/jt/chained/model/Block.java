package net.workshop.jt.chained.model;

import net.workshop.jt.chained.util.HashUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.function.Function;

public class Block {
    private static final Logger LOGGER = LoggerFactory.getLogger(Block.class);

    private String hash;
    private String previousHash;
    private String merkleRoot;
    //our data will be a simple message.
    private List<Transaction> transactions = new ArrayList<>();
    //as number of milliseconds since 1/1/1970.
    private LocalDateTime timestamp;
    private int nonce;

    public Block() {
    }

    //Block Constructor.
    public Block(Clock clock, String previousHash ) {
        this.previousHash = previousHash;
        this.timestamp = LocalDateTime.now(clock);

        this.hash = calculateHash(); //Making sure we do this after we set the other values.
    }

    //Calculate new hash based on blocks contents
    public String calculateHash() {
        return HashUtil.applySha256(
                previousHash +
                        timestamp +
                        Integer.toString(nonce) +
                        merkleRoot
        );
    }

    //Increases nonce value until hash target is reached.
    public void mineBlock(Function<Block, String> mineFunction) {
        this.hash = mineFunction.apply(this);
        LOGGER.debug("Block Mined!!! : " + hash);
    }

    //Add transactions to this block
    public void addTransaction(Transaction transaction) {
        //process transaction and check if valid, unless block is genesis block then ignore.
        if(transaction == null) return;
        if((previousHash.equals("0"))) {
            if((!transaction.processTransaction())) {
                LOGGER.debug("Transaction failed to process. Discarded.");
                return;
            }
        }
        transactions.add(transaction);
        LOGGER.debug("Transaction Successfully added to Block");
    }


    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getNonce() {
        return nonce;
    }

    public void setNonce(int nonce) {
        this.nonce = nonce;
    }

    public String getMerkleRoot() {
        return merkleRoot;
    }

    public void setMerkleRoot(String merkleRoot) {
        this.merkleRoot = merkleRoot;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Block.class.getSimpleName() + "[", "]")
                .add("hash='" + hash + "'")
                .add("previousHash='" + previousHash + "'")
                .add("merkleRoot='" + merkleRoot + "'")
                .add("transactions=" + transactions)
                .add("timestamp=" + timestamp)
                .add("nonce=" + nonce)
                .toString();
    }
}
