package net.workshop.jt.chained.entity;

import net.workshop.jt.chained.persistence.LocalDateTimeConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "BLOCK")
public class BlockEntity {

    @Id
    private String hash;

    @Column(name="PREVIOUS_HASH")
    private String previousHash;

    @Column(name="MERKLE_ROOT")
    private String merkleRoot;

    @Column(name="TIME_STAMP")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime timestamp;

    @Column(name="NONCE")
    private Integer nonce;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public String getMerkleRoot() {
        return merkleRoot;
    }

    public void setMerkleRoot(String merkleRoot) {
        this.merkleRoot = merkleRoot;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getNonce() {
        return nonce;
    }

    public void setNonce(Integer nonce) {
        this.nonce = nonce;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockEntity that = (BlockEntity) o;
        return Objects.equals(hash, that.hash) &&
                Objects.equals(previousHash, that.previousHash) &&
                Objects.equals(merkleRoot, that.merkleRoot) &&
                Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(nonce, that.nonce);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash, previousHash, merkleRoot, timestamp, nonce);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BlockEntity.class.getSimpleName() + "[", "]")
                .add("hash='" + hash + "'")
                .add("previousHash='" + previousHash + "'")
                .add("merkleRoot='" + merkleRoot + "'")
                .add("timestamp=" + timestamp)
                .add("nonce=" + nonce)
                .toString();
    }
}
