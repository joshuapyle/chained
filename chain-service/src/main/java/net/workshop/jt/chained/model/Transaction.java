package net.workshop.jt.chained.model;


import net.workshop.jt.chained.util.HashUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class Transaction {
    private static final Logger LOGGER = LoggerFactory.getLogger(Transaction.class);

    private String transactionId; // this is also the hash of the transaction.
    private PublicKey sender; // senders address/public key.
    private PublicKey recipient; // Recipients address/public key.
    private float value;
    private byte[] signature; // this is to prevent anybody else from spending funds in our wallet.

    private List<TransactionInput> inputs;
    private List<TransactionOutput> outputs = new ArrayList<>();

    private static int sequence = 0; // a rough count of how many transactions have been generated.

    // Constructor:
    public Transaction(PublicKey from, PublicKey to, float value, List<TransactionInput> inputs) {
        this.sender = from;
        this.recipient = to;
        this.value = value;
        this.inputs = inputs;
    }

    // This Calculates the transaction hash (which will be used as its Id)
    private String calulateHash() {
        sequence++; //increase the sequence to avoid 2 identical transactions having the same hash
        return HashUtil.applySha256(
                HashUtil.getStringFromKey(sender) +
                        HashUtil.getStringFromKey(recipient) +
                        Float.toString(value) + sequence
        );
    }

    //Returns true if new transaction could be created.
    public boolean processTransaction() {

        if (!verifiySignature()) {
            LOGGER.debug("#Transaction Signature failed to verify");
            return false;
        }

        //gather transaction inputs (Make sure they are unspent):
        for (TransactionInput i : inputs) {
            i.setUTXO(Chain.getUTXOs().get(i.getTransactionOutputId()));
        }

        //check if transaction is valid:
        if (getInputsValue() < Chain.MINIMUM_TRANSACTION) {
            LOGGER.debug("#Transaction Inputs to small: " + getInputsValue());
            return false;
        }

        //generate transaction outputs:
        float leftOver = getInputsValue() - value; //get value of inputs then the left over change:
        transactionId = calulateHash();
        outputs.add(new TransactionOutput(this.recipient, value, transactionId)); //send value to recipient
        outputs.add(new TransactionOutput(this.sender, leftOver, transactionId)); //send the left over 'change' back to sender

        //add outputs to Unspent list
        for (TransactionOutput transactionOutput : outputs) {
            Chain.getUTXOs().put(transactionOutput.getId(), transactionOutput);
        }

        //remove transaction inputs from UTXO lists as spent:
        for (TransactionInput transactionInput : inputs) {
            if (transactionInput.getUTXO() == null) continue; //if Transaction can't be found skip it
            Chain.getUTXOs().remove(transactionInput.getUTXO().getId());
        }

        return true;
    }

    //returns sum of inputs(UTXOs) values
    public float getInputsValue() {
        float total = 0;
        for (TransactionInput transactionInput : inputs) {
            if (transactionInput.getUTXO() == null) continue; //if Transaction can't be found skip it
            total += transactionInput.getUTXO().getValue();
        }
        return total;
    }

    //returns sum of outputs:
    public float getOutputsValue() {
        float total = 0;
        for (TransactionOutput o : outputs) {
            total += o.getValue();
        }
        return total;
    }

    //Signs all the data we don't wish to be tampered with.
    public byte[] generateSignature(PrivateKey privateKey) {
        String data = HashUtil.getStringFromKey(this.sender) + HashUtil.getStringFromKey(this.recipient) + Float.toString(this.value);
        return this.signature = HashUtil.applyECDSASig(privateKey, data);
    }

    //Verifies the data we signed hasnt been tampered with
    public boolean verifiySignature() {
        String data = HashUtil.getStringFromKey(this.sender) + HashUtil.getStringFromKey(this.recipient) + Float.toString(this.value);
        return HashUtil.verifyECDSASig(this.sender, data, this.signature);
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public PublicKey getSender() {
        return sender;
    }

    public void setSender(PublicKey sender) {
        this.sender = sender;
    }

    public PublicKey getRecipient() {
        return recipient;
    }

    public void setRecipient(PublicKey recipient) {
        this.recipient = recipient;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] signature) {
        this.signature = signature;
    }

    public List<TransactionInput> getInputs() {
        return inputs;
    }

    public void setInputs(ArrayList<TransactionInput> inputs) {
        this.inputs = inputs;
    }

    public List<TransactionOutput> getOutputs() {
        return outputs;
    }

    public void setOutputs(ArrayList<TransactionOutput> outputs) {
        this.outputs = outputs;
    }

    public static int getSequence() {
        return sequence;
    }

    public static void setSequence(int sequence) {
        Transaction.sequence = sequence;
    }
}