package net.workshop.jt.chained.service;

import ma.glasnost.orika.MapperFacade;
import net.workshop.jt.chained.entity.BlockEntity;
import net.workshop.jt.chained.model.Block;
import net.workshop.jt.chained.persistence.BlockRepository;
import net.workshop.jt.chained.util.MineUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.Clock;

@Service
public class BlockServiceImpl implements BlockService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlockServiceImpl.class);

    private final BlockRepository blockRepository;
    private final MapperFacade mapperFacade;

    public BlockServiceImpl(BlockRepository blockRepository, MapperFacade mapperFacade) {
        this.blockRepository = blockRepository;
        this.mapperFacade = mapperFacade;
    }

    @Override
    public Block initial(Clock clock){
        BlockEntity genesisEntity = blockRepository.findByPreviousHashIsNull();
        LOGGER.debug("genesisEntity found in repository: " + genesisEntity);

        Block genesis;
        if(genesisEntity == null){
            LOGGER.debug("Creating and Mining Genesis block... ");
            genesis = new Block( clock, "0");
            genesis.mineBlock(MineUtil.mineFunction);
            genesisEntity =  mapperFacade.map(genesis, BlockEntity.class);
            genesisEntity.setPreviousHash(null);
            blockRepository.save(genesisEntity);
        }else{
            genesis = mapperFacade.map(genesisEntity, Block.class);
            genesis.setPreviousHash("0");
        }

        return genesis;
    }
}
