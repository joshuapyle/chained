package net.workshop.jt.chained.controller;

import net.workshop.jt.chained.model.Wallet;
import net.workshop.jt.chained.service.WalletService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WalletController {

    private final WalletService walletService;

    public WalletController(WalletService walletService) {
        this.walletService = walletService;
    }

    @GetMapping("/wallet/{uuid}")
    public Wallet get(@PathVariable("uuid") String uuid){
        return walletService.getWallet(uuid);
    }

    @PutMapping("/wallet/{uuid}")
    public Wallet get(@PathVariable("uuid") String uuid, @RequestBody Wallet wallet){
        return walletService.update(uuid, wallet);
    }

    @PostMapping("/wallet")
    public Wallet create(@RequestBody Wallet wallet){
        return walletService.create(wallet);
    }

    @DeleteMapping("/wallet/{uuid}")
    public void delete(@PathVariable("uuid") String uuid){
        walletService.delete(uuid);
    }

}
