package net.workshop.jt.chained.service;

import net.workshop.jt.chained.model.Block;

import java.time.Clock;

public interface BlockService {

    Block initial(Clock clock);
}
