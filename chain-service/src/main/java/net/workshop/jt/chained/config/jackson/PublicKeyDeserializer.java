package net.workshop.jt.chained.config.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class PublicKeyDeserializer extends JsonDeserializer<PublicKey> {

    @Override
    public PublicKey deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        try {
            KeyFactory ecKeyFac = KeyFactory.getInstance("ECDSA", "BC");
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(p.getText()));
            return ecKeyFac.generatePublic(x509EncodedKeySpec);
        } catch (InvalidKeySpecException e) {
            throw new IOException("Invalid Key Spec", e);
        } catch (NoSuchAlgorithmException e) {
            throw new IOException("No Such Algorithm", e);
        } catch (NoSuchProviderException e) {
            throw new IOException("No ECDSA BC Provider", e);
        }
    }
}
