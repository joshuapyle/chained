package net.workshop.jt.chained.config.orika;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class PublicKeyConverter extends BidirectionalConverter<PublicKey, String> {

    @Override
    public String convertTo(PublicKey source, Type<String> destinationType, MappingContext mappingContext) {
        if(source == null){
            return null;
        }
        return Base64.getEncoder().encodeToString(source.getEncoded());
    }

    @Override
    public PublicKey convertFrom(String source, Type<PublicKey> destinationType, MappingContext mappingContext) {
        try {
            KeyFactory ecKeyFac = KeyFactory.getInstance("ECDSA", "BC");
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(source));
            return ecKeyFac.generatePublic(x509EncodedKeySpec);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("Invalid Key Spec", e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("No Such Algorithm", e);
        } catch (NoSuchProviderException e) {
            throw new RuntimeException("No ECDSA BC Provider", e);
        }
    }
}
