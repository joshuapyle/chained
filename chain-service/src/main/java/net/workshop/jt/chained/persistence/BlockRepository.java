package net.workshop.jt.chained.persistence;

import net.workshop.jt.chained.entity.BlockEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BlockRepository extends PagingAndSortingRepository<BlockEntity, String> {

    BlockEntity findByPreviousHashIsNull();

}
