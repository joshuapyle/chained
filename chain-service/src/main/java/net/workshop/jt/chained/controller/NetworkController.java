package net.workshop.jt.chained.controller;

import net.workshop.jt.chained.model.Block;
import net.workshop.jt.chained.service.BlockService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Clock;

@RestController
@RequestMapping("network")
public class NetworkController {

    private final BlockService blockService;

    public NetworkController(BlockService blockService) {
        this.blockService = blockService;
    }

    // JOIN

    // MAKE INITIAL BLOCK

    @GetMapping("initial")
    public Block initial(){
        return blockService.initial(Clock.systemUTC());
    }

    // ACCEPT BLOCK

    // CHECK - block is well formed

    // VALIDATE BLOCK -

    // VALIDATE TRANSACTION

    // VALIDATE TRANSACTION INPUT

    // CHECK TRANSACTION is well formed

    // APPLY BLOCK



}
