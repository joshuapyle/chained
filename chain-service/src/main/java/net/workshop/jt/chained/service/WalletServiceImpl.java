package net.workshop.jt.chained.service;

import ma.glasnost.orika.MapperFacade;
import net.workshop.jt.chained.entity.WalletEntity;
import net.workshop.jt.chained.exception.NotFoundException;
import net.workshop.jt.chained.model.Wallet;
import net.workshop.jt.chained.persistence.WalletRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final MapperFacade mapperFacade;

    public WalletServiceImpl(WalletRepository walletRepository, MapperFacade mapperFacade) {
        this.walletRepository = walletRepository;
        this.mapperFacade = mapperFacade;
    }

    @Override
    public Wallet createWallet() {
        Wallet wallet = new Wallet();
        wallet.setUuid(UUID.randomUUID().toString());
        wallet.generateKeyPair();

        WalletEntity walletEntity = mapperFacade.map(wallet, WalletEntity.class);
        walletRepository.save(walletEntity);

        return wallet;
    }

    @Override
    public Wallet getWallet(String uuid) {
        WalletEntity walletEntity = walletRepository.findById(uuid).orElseThrow(() -> new NotFoundException("Not Found"));
        return mapperFacade.map(walletEntity, Wallet.class);
    }

    @Override
    public void deleteWallet(String uuid) {
        walletRepository.deleteById(uuid);
    }

    @Override
    public Wallet update(String uuid, Wallet wallet) {
        WalletEntity walletEntity = walletRepository.findById(uuid).orElseThrow(() -> new NotFoundException("Not Found"));
        mapperFacade.map(wallet, walletEntity);
        walletEntity.setUuid(uuid);
        walletEntity =  walletRepository.save(walletEntity);
        return mapperFacade.map(walletEntity, Wallet.class);
    }

    @Override
    public Wallet create(Wallet wallet) {
        WalletEntity walletEntity = mapperFacade.map(wallet, WalletEntity.class);
        walletEntity.setUuid(UUID.randomUUID().toString());
        walletEntity = walletRepository.save(walletEntity);
        return mapperFacade.map(walletEntity, Wallet.class);
    }

    @Override
    public void delete(String uuid) {
        WalletEntity walletEntity = walletRepository.findById(uuid).orElseThrow(() -> new NotFoundException("Not Found"));
        walletRepository.delete(walletEntity);
    }
}
