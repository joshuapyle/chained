package net.workshop.jt.chained.persistence;

import net.workshop.jt.chained.entity.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {
}
