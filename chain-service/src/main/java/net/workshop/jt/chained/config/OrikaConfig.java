package net.workshop.jt.chained.config;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.builtin.PassThroughConverter;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.unenhance.HibernateUnenhanceStrategy;
import net.workshop.jt.chained.config.orika.PrivateKeyConverter;
import net.workshop.jt.chained.config.orika.PublicKeyConverter;
import net.workshop.jt.chained.entity.BlockEntity;
import net.workshop.jt.chained.entity.WalletEntity;
import net.workshop.jt.chained.model.Block;
import net.workshop.jt.chained.model.Wallet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Configuration
public class OrikaConfig {

    @Bean
    public MapperFactory mapperFactory(){

        MapperFactory mapperFactory = new DefaultMapperFactory.Builder()
                .unenhanceStrategy(new HibernateUnenhanceStrategy())
                .build();
        mapperFactory.getConverterFactory().registerConverter(new PassThroughConverter(LocalDate.class));
        mapperFactory.getConverterFactory().registerConverter(new PassThroughConverter(LocalDateTime.class));
        mapperFactory.getConverterFactory().registerConverter(new PublicKeyConverter());
        mapperFactory.getConverterFactory().registerConverter(new PrivateKeyConverter());

        mapperFactory.classMap(BlockEntity.class, Block.class)
                .field("hash", "hash")
                .field("previousHash", "previousHash")
                .field("merkleRoot", "merkleRoot")
                .field("timestamp", "timestamp")
                .field("nonce", "nonce")
                .byDefault()
                .register();

        mapperFactory.classMap(WalletEntity.class, Wallet.class)
                .field("uuid","uuid")
                .field("privateKey", "privateKey")
                .field("publicKey", "publicKey")
                .byDefault()
                .register();


        return mapperFactory;
    }

    @Bean
    public MapperFacade mapperFacade(MapperFactory mapperFactory){
        return mapperFactory.getMapperFacade();
    }
}
