package net.workshop.jt.chained.model;

import net.workshop.jt.chained.util.HashUtil;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.Security;

@RunWith(SpringRunner.class)
public class WalletTest {

    @BeforeClass
    public static void setup(){
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Test
    public void tst(){
        Wallet walletA = new Wallet();
        Wallet walletB = new Wallet();

        walletA.generateKeyPair();
        walletB.generateKeyPair();

        System.out.println("Private and public keys:");
        System.out.println(HashUtil.getStringFromKey(walletA.getPrivateKey()));
        System.out.println(HashUtil.getStringFromKey(walletA.getPublicKey()));

        Transaction transaction = new Transaction(walletA.getPublicKey(), walletB.getPublicKey(), 5, null);

        transaction.setSignature(transaction.generateSignature(walletA.getPrivateKey()));
        //Verify the signature works and verify it from the public key
        System.out.println("Is signature verified");
        System.out.println(transaction.verifiySignature());
    }
}
