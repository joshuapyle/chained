package net.workshop.jt.chained.config;

import ma.glasnost.orika.metadata.TypeBuilder;
import net.workshop.jt.chained.config.orika.PrivateKeyConverter;
import org.junit.Test;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class PrivateKeyConverterTest {

    private PrivateKey privateKey;

    private PrivateKeyConverter converter = new PrivateKeyConverter();

    {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        try {
            KeyFactory ecKeyFac = KeyFactory.getInstance("ECDSA", "BC");
            String keyString = "MHsCAQAwEwYHKoZIzj0CAQYIKoZIzj0DAQEEYTBfAgEBBBhkLjbCtV2uYNxDNrmA4ZH4ic9SwKfEwJagCgYIKoZIzj0DAQGhNAMyAASD2ZB4kWESznzYjUy/xunC2doOwZcUAnMvMpVzUweSqraxzeckj7AKZ5nLuFwWK7w=";
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(keyString));
            privateKey = ecKeyFac.generatePrivate(pkcs8EncodedKeySpec);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            throw new RuntimeException("Error Setting Up Key Pair", e);
        }
    }

    @Test
    public void fromByteToPrivateKey() {
        String str = Base64.getEncoder().encodeToString(privateKey.getEncoded());
        PrivateKey convertedKey = converter.convertFrom(str, new TypeBuilder<PrivateKey>() {}.build(), null);
        assertThat(privateKey, is(equalTo(convertedKey)));
    }

    @Test
    public void fromPrivateKeyToByte() {
        String convertedKey = converter.convertTo(privateKey, new TypeBuilder<String>() {}.build(), null);
        assertThat(Base64.getEncoder().encodeToString(privateKey.getEncoded()), is(equalTo(convertedKey)));
    }
}
