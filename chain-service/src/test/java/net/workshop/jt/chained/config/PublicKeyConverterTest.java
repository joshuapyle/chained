package net.workshop.jt.chained.config;

import ma.glasnost.orika.metadata.TypeBuilder;
import net.workshop.jt.chained.config.orika.PublicKeyConverter;
import org.junit.Test;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class PublicKeyConverterTest {

    private PublicKey publicKey;

    private PublicKeyConverter converter = new PublicKeyConverter();

    {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        try {
            KeyFactory ecKeyFac = KeyFactory.getInstance("ECDSA", "BC");
            String keyString = "MEkwEwYHKoZIzj0CAQYIKoZIzj0DAQEDMgAEgTk4zBapwvLlUTqUBx+mb0C4h7nJNYjteKXhObkVDVQK9VSRfHQNVY7XW1sMaLJ3";
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(keyString));
            publicKey = ecKeyFac.generatePublic(x509EncodedKeySpec);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException e) {
            throw new RuntimeException("Error Setting Up Key Pair", e);
        }
    }

    @Test
    public void fromByteToPublicKey() {
        String str = Base64.getEncoder().encodeToString(publicKey.getEncoded());
        PublicKey convertedKey = converter.convertFrom(str, new TypeBuilder<PublicKey>() {}.build(), null);
        assertThat(publicKey, is(equalTo(convertedKey)));
    }

    @Test
    public void fromPublicKeyToByte() {
        String convertedKey = converter.convertTo(publicKey, new TypeBuilder<String>() {}.build(), null);
        assertThat(Base64.getEncoder().encodeToString(publicKey.getEncoded()), is(equalTo(convertedKey)));
    }

}
