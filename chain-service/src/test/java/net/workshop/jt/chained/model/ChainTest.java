package net.workshop.jt.chained.model;

import net.workshop.jt.chained.config.OrikaConfig;
import net.workshop.jt.chained.persistence.BlockRepository;
import net.workshop.jt.chained.service.BlockService;
import net.workshop.jt.chained.service.BlockServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.Security;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {OrikaConfig.class, BlockServiceImpl.class})
public class ChainTest {

    @BeforeClass
    public static void setup(){
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    @Autowired
    private BlockService blockService;

    @MockBean
    private BlockRepository mockBlockRepository;

    @Test
    public void chain(){
        Chain chain = new Chain();

        LocalDateTime TEST_DATE_TIME = LocalDateTime.of(2016, 4, 1, 10, 0); //2016-04-01 at 10:00am
        ZoneId defaultZone = ZoneId.systemDefault();
        Clock clock = Clock.fixed(TEST_DATE_TIME.atZone(defaultZone).toInstant(), defaultZone);


        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()); //Setup Bouncey castle as a Security Provider

        //Create wallets:
        Wallet walletA = new Wallet();
        Wallet walletB = new Wallet();
        Wallet coinbase = new Wallet();

        walletA.generateKeyPair();
        walletB.generateKeyPair();
        coinbase.generateKeyPair();

        //create genesis transaction, which sends 100 NoobCoin to walletA:
        Transaction genesisTransaction = new Transaction(coinbase.getPublicKey(), walletA.getPublicKey(), 100f, new ArrayList<>());
        //manually sign the genesis transaction
        genesisTransaction.generateSignature(coinbase.getPrivateKey());
        //manually set the transaction id
        genesisTransaction.setTransactionId("0");
        //manually add the Transactions Output
        genesisTransaction.getOutputs().add(new TransactionOutput(genesisTransaction.getRecipient(), genesisTransaction.getValue(), genesisTransaction.getTransactionId()));
        //its important to store our first transaction in the UTXOs list.
        Chain.getUTXOs().put(genesisTransaction.getOutputs().get(0).getId(), genesisTransaction.getOutputs().get(0));

        System.out.println("Creating and Mining Genesis block... ");
        Block genesis = blockService.initial(clock);
        genesis.addTransaction(genesisTransaction);
        chain.addBlock(genesis);

        //testing
        Block block1 = new Block(clock, genesis.getHash());
        System.out.println("\nWalletA's balance is: " + walletA.getBalance());
        System.out.println("\nWalletA is Attempting to send funds (40) to WalletB...");
        block1.addTransaction(walletA.sendFunds(walletB.getPublicKey(), 40f));
        chain.addBlock(block1);
        System.out.println("\nWalletA's balance is: " + walletA.getBalance());
        System.out.println("WalletB's balance is: " + walletB.getBalance());

        Block block2 = new Block(clock, block1.getHash());
        System.out.println("\nWalletA Attempting to send more funds (1000) than it has...");
        block2.addTransaction(walletA.sendFunds(walletB.getPublicKey(), 1000f));
        chain.addBlock(block2);
        System.out.println("\nWalletA's balance is: " + walletA.getBalance());
        System.out.println("WalletB's balance is: " + walletB.getBalance());

        Block block3 = new Block(clock, block2.getHash());
        System.out.println("\nWalletB is Attempting to send funds (20) to WalletA...");
        block3.addTransaction(walletB.sendFunds( walletA.getPublicKey(), 20));
        System.out.println("\nWalletA's balance is: " + walletA.getBalance());
        System.out.println("WalletB's balance is: " + walletB.getBalance());

        chain.isChainValid(genesisTransaction);
    }

}
