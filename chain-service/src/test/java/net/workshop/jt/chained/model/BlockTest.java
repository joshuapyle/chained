package net.workshop.jt.chained.model;

import net.workshop.jt.chained.util.MineUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

@RunWith(SpringRunner.class)
public class BlockTest {

    @Test
    public void chain(){
        LocalDateTime TEST_DATE_TIME = LocalDateTime.of(2016, 4, 1, 10, 0); //2016-04-01 at 10:00am
        ZoneId defaultZone = ZoneId.systemDefault();
        Clock clock = Clock.fixed(TEST_DATE_TIME.atZone(defaultZone).toInstant(), defaultZone);

        Block genesisBlock = new Block(clock,"0");

        genesisBlock.mineBlock(MineUtil.mineFunction);
        System.out.println("StringUtils.length(genesisBlock.getHash()) = " + StringUtils.length(genesisBlock.getHash()));
        System.out.println("Hash for block 1 : " + genesisBlock.getHash());

        Block secondBlock = new Block(clock,genesisBlock.getHash());
        secondBlock.mineBlock(MineUtil.mineFunction);
        System.out.println("Hash for block 2 : " + secondBlock.getHash());

        Block thirdBlock = new Block(clock, secondBlock.getHash());
        thirdBlock.mineBlock(MineUtil.mineFunction);
        System.out.println("Hash for block 3 : " + thirdBlock.getHash());

    }

}
