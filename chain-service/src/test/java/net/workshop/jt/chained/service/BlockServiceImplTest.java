package net.workshop.jt.chained.service;

import ma.glasnost.orika.MapperFacade;
import net.workshop.jt.chained.config.OrikaConfig;
import net.workshop.jt.chained.entity.BlockEntity;
import net.workshop.jt.chained.model.Block;
import net.workshop.jt.chained.persistence.BlockRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {OrikaConfig.class, BlockServiceImplTest.TestContext.class})
@TestPropertySource("/test.properties")
public class BlockServiceImplTest {


    private static final String GENESIS_HASH = "000003b327b5e9e585ed09e97ec674cd252bb37fc73ea968a011e0d3b113aed0";
    private static final int GENESIS_NONCE = 52625;
    private static final String GENESIS_PREV_HASH = "0";

    @Autowired
    private BlockService blockService;

    @MockBean
    private BlockRepository mockBlockRepository;

    @Before
    public void reset(){
        Mockito.reset(mockBlockRepository);
    }

    @Test
    public void createInitialBlock(){
        //2016-04-01 at 10:00am
        LocalDateTime TEST_DATE_TIME = LocalDateTime.of(2016, 4, 1, 10, 0);
        ZoneId defaultZone = ZoneId.systemDefault();
        Clock clock = Clock.fixed(TEST_DATE_TIME.atZone(defaultZone).toInstant(), defaultZone);
        Block genesisBlock = blockService.initial(clock);

        assertThat(genesisBlock.getHash(), is(equalTo(GENESIS_HASH)));
        assertThat(genesisBlock.getNonce(), is(equalTo(GENESIS_NONCE)));
        assertThat(genesisBlock.getPreviousHash(), is(equalTo(GENESIS_PREV_HASH)));

        verify(mockBlockRepository, times(1)).findByPreviousHashIsNull();
        verify(mockBlockRepository, times(1)).save(any(BlockEntity.class));
    }

    @Test
    public void getInitialBlockFromDb(){
        BlockEntity testEntity = new BlockEntity();
        testEntity.setHash(GENESIS_HASH);
        testEntity.setNonce(GENESIS_NONCE);
        testEntity.setPreviousHash(GENESIS_PREV_HASH);
        when(mockBlockRepository.findByPreviousHashIsNull()).thenReturn(testEntity);

        //2016-04-01 at 10:00am
        LocalDateTime TEST_DATE_TIME = LocalDateTime.of(2016, 4, 1, 10, 0);
        ZoneId defaultZone = ZoneId.systemDefault();
        Clock clock = Clock.fixed(TEST_DATE_TIME.atZone(defaultZone).toInstant(), defaultZone);
        Block genesisBlock = blockService.initial(clock);

        assertThat(genesisBlock.getHash(), is(equalTo(GENESIS_HASH)));
        assertThat(genesisBlock.getNonce(), is(equalTo(GENESIS_NONCE)));
        assertThat(genesisBlock.getPreviousHash(), is(equalTo(GENESIS_PREV_HASH)));

        verify(mockBlockRepository, times(1)).findByPreviousHashIsNull();
        verify(mockBlockRepository, times(0)).save(any(BlockEntity.class));
    }

    @Configuration
    static class TestContext{

        @Bean
        public BlockService blockService(BlockRepository blockRepository,
                                         MapperFacade mapperFacade){
            return new BlockServiceImpl(blockRepository, mapperFacade);
        }

    }


}
