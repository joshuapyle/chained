package net.workshop.jt.chained.auth.security;

public class SecurityConstants {
	// TODO Change the Secret to be a base64 encoded private key that comes from application.properties
	public static final String SECRET = "SecretKeyToGenJWTs";
	// TODO Consider moving all of these to application.properties
	public static final long EXPIRATION_TIME = 864_000_000; // 10 days
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String SIGN_UP_URL = "/user/sign-up";
}
