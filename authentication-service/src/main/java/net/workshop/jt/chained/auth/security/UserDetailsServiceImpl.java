package net.workshop.jt.chained.auth.security;

import net.workshop.jt.chained.auth.model.UserEntity;
import net.workshop.jt.chained.auth.persistence.UserEntityRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

@Service
@Primary
public class UserDetailsServiceImpl  implements UserDetailsService {
	private UserEntityRepository userEntityRepository;

	public UserDetailsServiceImpl(UserEntityRepository userEntityRepository) {
		this.userEntityRepository = userEntityRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserEntity userEntity = userEntityRepository.findByEmailIgnoreCase(email);
		if (userEntity == null) {
			throw new UsernameNotFoundException(email);
		}
		return new User(userEntity.getEmail(), userEntity.getPassword(), emptyList());
	}
}
