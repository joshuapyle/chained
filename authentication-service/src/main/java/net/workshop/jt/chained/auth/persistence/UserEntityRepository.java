package net.workshop.jt.chained.auth.persistence;

import net.workshop.jt.chained.auth.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserEntityRepository extends JpaRepository<UserEntity, String> {
    UserEntity findByEmailIgnoreCase(String username);
}
