package net.workshop.jt.chained.auth.controller;

import net.workshop.jt.chained.auth.domain.User;
import net.workshop.jt.chained.auth.model.UserEntity;
import net.workshop.jt.chained.auth.persistence.UserEntityRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.function.Supplier;

@RestController
@RequestMapping("/user")
public class UserController {

	private final UserEntityRepository userEntityRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserController(UserEntityRepository userEntityRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.userEntityRepository = userEntityRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}

	@GetMapping("{uuid}")
    public User getByUuid(@PathVariable String uuid){

	    // TODO Use Orika
        // TODO Check security and make sure user is only getting their details...

	    User user = new User();

        UserEntity userEntity = userEntityRepository.findById(uuid)
                .orElseThrow(() -> new RuntimeException("Not Found"));
        user.setEmail(userEntity.getEmail());
        user.setUuid(userEntity.getUuid());

	    return user;
    }

	@PostMapping("/sign-up")
	public void signUp(@RequestBody UserEntity user) {
		user.setUuid(UUID.randomUUID().toString());
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setEmail(StringUtils.lowerCase(user.getEmail()));
		userEntityRepository.save(user);
	}
}
